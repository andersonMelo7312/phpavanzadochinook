<!--Extender la masterpage en esta vista--> 
@extends('layouts.masterpage')

@section('contenido')
        <h1>Lista Empleados</h1>
        <table class="table table borderless">
        <thead></thead>
        <tr>
            <th>Nombre y apellido</th>
            <th>Cargo</th>            
            <th>Email</th>
            <th class="text-danger">Ver Detalles</th>
        </tr>
        <tbody>
            @foreach($empleados as $empleado)
            <tr>
                <td><strong class="text-danger"> {{ $empleado->FirstName }}</strong> 
                {{ $empleado->LastName }}  
                </td>
                <td> {{$empleado->Title}} </td>                
                <td> {{$empleado->Email}} </td>
               <td> <a class="btn btn-primary" href="{{url('empleados/'.$empleado->EmployeeId) }}"> Ver informacion</a></td>
                
            </tr>
            @endforeach
        </tbody>
        <tbody></tbody>
    </table>    
    {{ $empleados->links()  }}
@endsection