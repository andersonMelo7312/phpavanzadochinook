<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleados extends Model
{
    protected $table = "employees";
    protected $primaryKey = "EmployeeId";
    public $timestamps = false;

    //establecer algunos en adelante
    //el - eloquent : trata con objetos carbon

    protected $dates=['BirthDate' , 'HireDate'];    
    public function jefe_directo(){
        return $this->belongsTo('App\Empleados', 'ReportsTo');
    }

    public function cliente(){

        return $this->hasMany("App\cliente" , 'SupportRepId');
    }
}
    

