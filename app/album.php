<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class album extends Model
{
    protected $table = "albums";
    protected $primaryKey = "AlbumId";
    public $timestamps = false;
    
    public function Artista(){
        return $this->belongsTo('App\Artista', 'ArtisId');
    }
    
}
