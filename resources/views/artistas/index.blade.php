<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha512-mG7Xo6XLlQ13JGPQLgLxI7bz8QlErrsE9rYQDRgF+6AlQHm9Tn5bh/vaIKxBmM9mULPC6yizAhEmKyGgNHCIvg==" crossorigin="anonymous" />
    <title>Document</title>
</head>
<body>
    <h1> Lista de Artistas </h1>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>
                    Nombre
                </th>      
            </tr>
        </thead>
        <tbody>
            @foreach($Artistas as $artista )
            <tr>
                <td>
                    {{ $artista->Name }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>

