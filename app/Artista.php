<?php

namespace App;

use Illuminate\Database\Eloquent\model;

class Artista extends model
{
    protected $table = "artists";
    protected $primaryKey = "ArtistId";
    public $timestamps = false;

    //relacion artista-album
    //metodos  albunes me va a devolver los albunes del artista

    public function albumes(){
        return $this->hasMany('App\Album' , 'ArtistId');
    }
}