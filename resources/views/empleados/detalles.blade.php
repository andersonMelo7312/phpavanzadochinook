<!--Extender la masterpage en esta vista--> 
@extends('layouts.masterpage')

@section('contenido')
<h2>Informacion del Empleado</h2>
  <br/>
  <div class="card" style="width:400px">

    
    <div class="card" style="card-header">
    
    
      <h4 class="card-title">{{ $empleado->FirstName}} {{$empleado->LastName}}</h4>
    </div>
      
    <div class="card" style="card-header">
      <h4 class="card-title">{{ $empleado->FirstName}} {{$empleado->LastName}}</h4>
    </div calss="card-body">

    <!--info de empleado en list item-->
    <ul class="list-group">
    <li class="list-group-item text-secondary"> {{$empleado->Title}} </li>

  @if( $empleado->jefe_directo()->get()->count()!==0)

    <li class="list-group-item text-secondary">
        jefe Directo:
        {{ $empleado->jefe_directo()->first()->FirstName }}
        {{ $empleado->jefe_directo()->first()->LastName }}  
    </li>
    @endif    
    <li class="list-group-item text-success">
      fecha de nacimiento:{{ $empleado->BirthDate->format('l jS \\of F Y h:i:s A')}}
    </li> 
    <li class="list-group-item text-success text-bold">
      fecha de Contratacion:{{ $empleado->HireDate->format('l jS \\of F Y h:i:s A')}}
    </li> 
    
    <br/>
    </ul>  
      
  </div>   
</div>
@if ( $empleado->cliente()->get()->count() !==0 ):
<h3>Clientes atendidos</h3>
<table class="table table-striped">
  <thead>
    <tr>
      <th>Nombre</th>
      <th>Compañia</th>
      <th>Email</th>      
    </tr>
  </thead>
  <tbody>
    @foreach ($empleado->cliente()->get() as $cliente )
    <tr>
      <td>{{  $cliente->FirstName }}  {{ $cliente->LastName }} </td>
      @if( $cliente->Company )
        <td>{{ $cliente->Company}}</td>
      @else
        <td><strong class="text-danger"> INDEPENDIENTE </strong></td>
      @endif    
      <td>{{ $cliente->Email }} </td>
    </tr>

    @endforeach
  </tbody>

</table>
@else

@endif
@endsection